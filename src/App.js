import './App.css';
import { Route, Routes } from 'react-router-dom';

import RecipeList from './components/RecipeList';
import RecipeDetail from './components/RecipeDetail';
import Header from './components/Header';
import Footer from './components/Footer';

function App() {
  return (
    <div>
      <Header />
      <Routes >
        <Route path="/" element={<RecipeList />} />
        <Route path="/:id" element={<RecipeDetail />} />
      </Routes>
      <Footer />

    </div>

  );
}

export default App;