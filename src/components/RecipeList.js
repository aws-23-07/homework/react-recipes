import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

const RecipeList = () => {
  const [recipes, setRecipes] = useState([]);

  useEffect(() => {
    fetch('http://localhost:3001/recipes')
      .then((res) => res.json())
      .then((data) => setRecipes(data));
  }, []);

  return (
    <main className="p-4">
      <h1 className="text-2xl font-bold mb-4">Recipes</h1>
      <ul className="space-y-4">
        {recipes.map((recipe) => (
          <li key={recipe.id}>
            <Link to={`/${recipe.id}`} className="flex items-center space-x-2">
              <img src={recipe.image} alt={recipe.name} className="w-[200px] h-20 object-cover rounded-md" />
              <div>
                <h2 className="text-lg font-bold">{recipe.name}</h2>
              </div>
            </Link>
          </li>
        ))}
      </ul>
    </main>
  );
};

export default RecipeList;