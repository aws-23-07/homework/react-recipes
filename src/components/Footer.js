import React from 'react';

const Footer = () => {
  return (
    <footer className="bg-gray-200 text-gray-800 p-4">
      <p className="text-center">Copyleft © 2024 My Homework</p>
    </footer>
  );
};

export default Footer;